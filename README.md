
# leaflet-control-chart

[Leaflet](https://leafletjs.com/) Chart Control

leaflet-control-chart uses [Plotly.js](https://plotly.com/javascript/)
for data visualization.

# NPM

This package is available on [npm](https://www.npmjs.com/):

[@lowswaplab/leaflet-control-chart](https://www.npmjs.com/package/@lowswaplab/leaflet-control-chart)

This package can be installed by performing:

    npm install @lowswaplab/leaflet-control-chart

# JavaScript

Example, with options (and defaults):

    import "@lowswaplab/leaflet-control-chart";

    L.control.chart(
      {
      isOpen: false,        // start out open or closed?
      iconOpen: "📊",       // open icon UTF-8 0x1F4CA "Bar Chart"
      iconClose: "✖",       // close icon UTF-8 0x1F7A# "Heavy Multiplication X"
      }).addTo(map);

# Source Code

[leaflet-control-chart](https://gitlab.com/lowswaplab/leaflet-control-chart)

# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).

# Author

[Low SWaP Lab](https://www.lowswaplab.com/)

# Copyright

Copyright © 2020 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

