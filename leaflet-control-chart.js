
// Leaflet Chart Control

// https://gitlab.com/lowswaplab/leaflet-control-chart

// Copyright © 2020 Low SWaP Lab lowswaplab.com

// https://plotly.com/javascript/
import Plotly from "plotly.js/dist/plotly-gl3d.js";

(function()
  {
  L.Control.Chart = L.Control.extend(
    {
    _plotly: undefined,
    _button: undefined,
    _canvas: undefined,
    _ctx: undefined,
    _chartLayout: undefined,
    _chartConfig: undefined,
    _mouseDown: false,

    options:
      {
      startOpen: false,     // start out open or closed?
      iconOpen: "📊",       // open icon UTF-8 0x1F4CA "Bar Chart"
      iconClose: "✖",       // close icon UTF-8 0x1F7A "Heavy Multiplication X"
      zIndex: 1000
      },

    initialize: function(options)
      {
      L.setOptions(this, options);

      this._chartLayout =
        {
        margin:
          {
          r: 0,
          r: 0,
          b: 0,
          t: 0
          }
        };
      },

    onAdd: function(map)
      {
      var _this=this;
      this._container = L.DomUtil.create("div", "leaflet-control-chart");
      this._container.id = "leaflet-control-chart";
      this._container.style.width = "50vw";
      this._container.style.height = "auto";
      this._container.style.position = "relative";
      this._container.style.padding = "2px";
      this._container.style.color = "#000";
      this._container.style.backgroundColor = "#FFF";
      this._container.style.border = "2px solid rgba(0, 0, 0, 0.2)";
      this._container.style.borderRadius = "5px";
      this._container.style.boxShadow = "0 1px 7px rgba(0, 0, 0, 0.4)";
      this._container.style.cursor = "pointer";
      this._button = L.DomUtil.create("div", "leaflet-control-chart-button");
      this._button.id = "leaflet-control-chart-button";
      this._button.style.position = "absolute";
      this._button.style.margin = "0px";
      this._button.style.padding = "0px";
      this._button.style.left = "auto";
      this._button.style.right = "0px";
      this._button.style.height = "32px";
      this._button.style.width = "28px";
      this._button.style.textAlign = "center";
      this._button.onclick = function()
        {
        _this._closeToggle();
        };

      this._canvas = L.DomUtil.create("canvas",
        "leaflet-control1-chart-canvas");
      this._canvas.style.margin = "4px 15px 2px 2px"; // room for button
      this._canvas.style.padding = "4px 15px 2px 2px"; // room for button
      this._ctx = this._canvas.getContext("2d");

      this._container.appendChild(this._canvas);
      this._container.appendChild(this._button);

      // Plotly.newPlot() needs the element to actually exist
      // Leaflet will move it to its proper position later
      document.body.appendChild(this._container);

      L.DomEvent.disableClickPropagation(this._container);
      L.DomEvent.on(this._container, "mousewheel", L.DomEvent.stopPropagation);
      L.DomEvent.on(this._container, "click", L.DomEvent.stopPropagation);

      this._container.addEventListener("mousedown", function(evt)
         {
         _this._mouseDown = true;
         });
      document.addEventListener("mouseup", function(evt)
         {
         _this._mouseDown = false;
         });

      this._closeToggle(); // close and set various button parameters
      if (this.options.startOpen)
        this._closeToggle(); // repoen

      this._plotly = Plotly.newPlot("leaflet-control-chart", [],
        this._chartLayout, this._chartConfig);

      return(this._container);
      },

    onRemove: function(map)
      {
      // XXX ?
      },

    _closeToggle: function()
      {
      if (this._canvas.style.display === "none")
        {
        this._container.style.padding = "2px";
        this._container.style.width = "50vw";
        this._container.style.height = "50vh";

        this._button.innerHTML = this.options.iconClose;
        this._button.style.top = "0px";
        this._button.style.fontSize = "16px";

        this._canvas.style.display = "block";
        }
      else
        {
        this._canvas.style.display = "none";

        this._container.style.padding = "0px";
        this._container.style.width = "28px";
        this._container.style.height = "28px";

        this._button.innerHTML = this.options.iconOpen;
        this._button.style.top = "-2px";
        this._button.style.fontSize = "24px";
        };
      }

    });

  L.Map.mergeOptions(
    {
    positionControl: false
    });

  L.Map.addInitHook(function ()
    {
    if (this.options.positionControl)
      {
      this.positionControl = new L.Control.Chart();
      this.addControl(this.positionControl);
      };
    });

  L.control.chart = function (options)
    {
    return(new L.Control.Chart(options));
    };
  }
)();

